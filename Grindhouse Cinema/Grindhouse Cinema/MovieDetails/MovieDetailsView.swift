//
//  MovieDetailsView.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 20/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import UIKit

class MovieDetailsView: UIView {
    // outlets are connected to the TableViewCell
    @IBOutlet var moviePoster: UIImageView!
    @IBOutlet var movieOverview: UILabel!
    @IBOutlet var favouriteMovie: UIButton!
}
