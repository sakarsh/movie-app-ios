//
//  MovieDetailsViewController.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 20/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import UIKit
import SDWebImage

class MovieDetailsViewController: UIViewController {
    // vairables to the movie id and details of the movie
    var movieId: Int?
    var movieDetails: MovieDetails?
    // reference outlet to the movie details view
    @IBOutlet var movieDetailsLayout: MovieDetailsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    // setting up the UI of the ViewController
    func setupUI() {
        // string interpolation of the search query to fetch details of the movie
        guard let movieDetailsURL = URL(string: "https://api.themoviedb.org/3/movie/\(movieId ?? 0)?api_key=9e815367d4692859a98bd5557cb72440") else { return }
        // executing the method to get the results of the movie details
        FindMovie.fetchMovieDetails(movieDetailsURL, completionHandler: { movieDetails, Error in
            if let movieDetails = movieDetails {
                self.movieDetails = movieDetails
                self.setMovieDetails()
            }
        })
    }
    // setting up the movie details in the ViewController
    func setMovieDetails() {
        if movieId != nil {
            // string interpolation of imageURL
            guard let imageURL = URL(string: "https://image.tmdb.org/t/p/w185_and_h278_bestv2\(self.movieDetails?.poster_path ?? "")") else { fatalError("Failed while casting image URL") }
            // using sdwebimage library to set the image to the imageView
            movieDetailsLayout.moviePoster.sd_setImage(with: imageURL)
            DispatchQueue.main.async {
                self.movieDetailsLayout.movieOverview.text = self.movieDetails?.overview
            }
            // setting the movie title
            DispatchQueue.main.async {
                self.title = self.movieDetails?.original_title
            }
            checkFavourites()
        }
    }
    // checking if the movie to be displayed in this ViewController
    // is a favourite movie or not
    func checkFavourites() {
        let manager = CoreDataManager()
        if let movieId = movieDetails?.id {
            // executing the method isFavourite to check if the movie
            // is present in the favourite movies list
            let isFavourite = manager.isFavourite(with: movieId)
            DispatchQueue.main.async {
                self.movieDetailsLayout.favouriteMovie.addTarget(self, action: #selector(self.favouriteButtonClicked), for: .touchUpInside)
            }
            // condition to display the heart on and off based on the result
            if isFavourite {
                DispatchQueue.main.async {
                    self.movieDetailsLayout.favouriteMovie.setImage(UIImage.init(named: "heart-on"), for: .normal)
                }
            } else {
                DispatchQueue.main.async {
                    self.movieDetailsLayout.favouriteMovie.setImage(UIImage.init(named: "heart-off"), for: .normal)
                }
            }
        }
    }
    // button click method to add the movie into the favourite movies list
    // or remove it from the favourite movies list
    @objc func favouriteButtonClicked(_ sender: UIButton) {
        if !sender.isSelected {
            let manager = CoreDataManager()
            if let movieId = movieDetails?.id, let movieName = movieDetails?.original_title, let moviePoster = movieDetails?.poster_path {
                // executing the method isFavourite to check if the movie
                // is present in the favourite movies list
                let isFavourite = manager.isFavourite(with: movieId)
                // condition to display the heart on and off based on the result
                if isFavourite {
                    // executing the method removeFavourite to remove the
                    // movie from the favourite movie list
                    manager.removeFavourite(by: movieId)
                    movieDetailsLayout.favouriteMovie.setImage(UIImage.init(named: "heart-off"), for: .normal)
                } else {
                    // executing the method addFavourite to add the
                    // movie to the favourite movie list
                    manager.addFavourite(by: movieId, name: movieName, imageURL: moviePoster)
                    movieDetailsLayout.favouriteMovie.setImage(UIImage.init(named: "heart-on"), for: .normal)
                }
            }
        }
    }
}
