//
//  MovieDetails.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 20/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import Foundation

struct MovieDetails: Codable {
    var adult: Bool?
    var backdrop_path: String?
    var belongs_to_collection: belongs_to_collection?
    var budget: Int?
    var genres: [genres]?
    var homepage: String?
    var id: Int?
    var imdb_id: String?
    var original_language: String?
    var original_title: String?
    var overview: String?
    var popularity: Double?
    var poster_path: String?
    var production_companies: [production_companies]?
    var production_countries: [production_countries]?
    var release_date: String?
    var revenue: Int?
    var runtime: Int?
    var spoken_languages: [spoken_languages]?
    var status: String?
    var tagline: String?
    var title: String?
    var video: Bool?
    var vote_average: Double?
    var vote_count: Int?
}

struct belongs_to_collection: Codable {
    var id: Int?
    var name: String?
    var poster_path: String?
    var backdrop_path: String?
}

struct genres: Codable {
    var id: Int?
    var name: String?
}

struct production_companies: Codable {
    var id: Int?
    var logo_path: String?
    var name: String?
    var origin_country: String?
}

struct production_countries: Codable {
    var iso_3166_1: String?
    var name: String?
}

struct spoken_languages: Codable {
    var iso_639_1: String?
    var name: String?
}
