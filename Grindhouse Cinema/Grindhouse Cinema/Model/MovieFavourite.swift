//
//  MovieFavourite.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 22/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import Foundation

struct MovieFavourite {
    var movieId: Int?
    var movieName: String?
    var moviePoster: String?
}
