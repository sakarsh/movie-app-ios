//
//  MovieSearch.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 18/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import Foundation

struct MovieSearch: Codable {
    var page: Int
    var total_results: Int
    var total_pages: Int
    var results: [results]
}

struct results: Codable {
    var vote_count: Int?
    var id: Int?
    var video: Bool?
    var vote_average: Double?
    var title: String?
    var popularity: Double?
    var poster_path: String?
    var original_language: String?
    var original_title: String?
    var genre_ids: [Int]?
    var backdrop_path: String?
    var adult: Bool?
    var overview: String?
    var release_date: String?
}
