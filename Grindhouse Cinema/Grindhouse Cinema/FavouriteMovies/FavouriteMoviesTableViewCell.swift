//
//  FavouriteMoviesTableViewCell.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 21/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import UIKit

class FavouriteMoviesTableViewCell: UITableViewCell {
    // outlets are connected to the TableViewCell
    @IBOutlet var moviePoster: UIImageView!
    @IBOutlet var movieName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
