//
//  FavouriteMoviesTableViewController.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 21/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class FavouriteMoviesTableViewController: UITableViewController {
    // array is declared to store the result of the favourite movies
    var favouriteMovies: [MovieFavourite]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFavouriteMovies()
        // setting the title of the layout to favourite movies
        self.title = "Favourite Movies"
        // initializing the properties
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getFavouriteMovies()
        self.tableView.reloadData()
    }
    // gets the favourite movies from the database
    // sets the result to the declared variable favouriteMovies
    fileprivate func getFavouriteMovies() {
        let manager = CoreDataManager()
        manager.getFavourite(completionHandler: { favouriteMovies, Error in
            if let favouriteMovies = favouriteMovies {
                self.favouriteMovies = favouriteMovies
            }
        })
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteMovies?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "favouriteMoviescell", for: indexPath) as? FavouriteMoviesTableViewCell else {
            fatalError("DequeueReusableCell failed while casting")
        }
        // string interpolation of imageURL
        guard let imageURL = URL(string: "https://image.tmdb.org/t/p/w185_and_h278_bestv2\(favouriteMovies?[indexPath.row].moviePoster ?? "")") else { fatalError("Failed while casting image URL") }
        // setting the movie title
        cell.movieName.text = favouriteMovies?[indexPath.row].movieName
        // using sdwebimage library to set the image to the imageView
        cell.moviePoster.sd_setImage(with: imageURL)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "MovieDetailsLayout") as? MovieDetailsViewController else {
            fatalError("Failed while casting the MovieDetailsVC")
        }
        // passing the movieId from this ViewController to MovieDetailsViewController
        viewController.movieId = favouriteMovies?[indexPath.row].movieId ?? 0
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
