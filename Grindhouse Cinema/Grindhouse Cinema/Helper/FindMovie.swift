//
//  FindMovie.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 21/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import Foundation

class FindMovie {
    // method to fetch movie details and return movie details results
    static func fetchMovieDetails(_ url: URL, completionHandler: @escaping (MovieDetails?, Error?) -> Void) {
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                // parsing the JSON results and storing the parsed data
                let movieDetails = try jsonDecoder.decode(MovieDetails.self, from: dataResponse)
                completionHandler(movieDetails, nil)
            } catch {
                print(error)
                completionHandler(nil, error)
            }
        }
        dataTask.resume()
    }
    // method to fetch the list of movies data and return the list of movies results
    static func fetchMovieData(_ url: URL, completionHandler: @escaping (MovieSearch?, Error?) -> Void) {
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                // parsing the JSON results and storing the parsed data
                let movieSearch  = try jsonDecoder.decode(MovieSearch.self, from: dataResponse)
                completionHandler(movieSearch, nil)
            } catch {
                print(error)
                completionHandler(nil, error)
            }
        }
        dataTask.resume()
    }
}
