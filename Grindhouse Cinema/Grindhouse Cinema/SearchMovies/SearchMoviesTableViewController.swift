//
//  SearchMoviesTableViewController.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 22/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import UIKit
import SDWebImage

class SearchMoviesTableViewController: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating {
    // outlet referenced to the searchBar in storyboard
    @IBOutlet var searchBar: UISearchBar!
    // variable declared to store the result of find movies
    var movieSearch: MovieSearch?
    // previousRunDate stores the current date
    private var previousRunDate = Date()
    // setting the minimum time interval for every search query
    private var minimumTimeInterval = 0.05
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // initializing the properties
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        // enable showing the cancel Button next to the searchBar
        self.searchBar.showsCancelButton = true
        // Setting the definesPresentationContext to true so that
        // the search bar will remain in this ViewController
        definesPresentationContext = true
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movieSearch?.results.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "searchMoviesCell", for: indexPath) as? SearchMoviesTableViewCell else {
            fatalError("DequeueReusableCell failed while casting")
        }
        // string interpolation of imageURL
        guard let imageURL = URL(string: "https://image.tmdb.org/t/p/w185_and_h278_bestv2\(self.movieSearch?.results[indexPath.row].poster_path ?? "")") else { fatalError("Failed while casting image URL") }
        // using sdwebimage library to set the image to the imageView
        cell.moviePoster.sd_setImage(with: imageURL)
        // setting the movie title
        cell.movieName.text = self.movieSearch?.results[indexPath.row].original_title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "MovieDetailsLayout") as? MovieDetailsViewController else {
            fatalError("Failed while casting the MovieDetailsVC")
        }
        // passing the movieId from this ViewController to MovieDetailsViewController
        viewController.movieId = movieSearch?.results[indexPath.row].id ?? 0
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        movieSearch = nil
        // assing the constant search query to the text from the search bar
        guard let searchQuery = searchBar.text else {
            return
        }
        // comparing the time interval since the previous time internval and
        // then executing the method fetchResults for the search query
        if Date().timeIntervalSince(previousRunDate) > minimumTimeInterval {
            previousRunDate = Date()
            fetchResults(for: searchQuery)
        }
    }
    // method to fetch results based on the search query
    func fetchResults(for searchQuery: String) {
        // string interpolation of the search query to find a movie
        guard let movieSearchURL = URL(string: "https://api.themoviedb.org/3/search/movie?api_key=9e815367d4692859a98bd5557cb72440&query=\(searchQuery)") else { return }
        // method to get the results and assign the results to the vairable
        FindMovie.fetchMovieData(movieSearchURL, completionHandler: { movieSearch, Error in
            if let movieSearch = movieSearch {
                self.movieSearch = movieSearch
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // setting the results variable to nil in order to empty the results data
        movieSearch = nil
        self.tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        self.tableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.tableView.reloadData()
    }
}
