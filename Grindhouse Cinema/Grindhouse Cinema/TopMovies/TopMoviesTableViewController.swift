//
//  TopMoviesTableViewController.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 17/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage

class TopMoviesTableViewController: UITableViewController {
    // variable declared to store the result of find movies
    var movieSearch: MovieSearch?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarButtons()
        // setting the title of the layout to Grindhouse Cinema
        self.title = "Grindhouse Cinema"
        setupUI()
        // initializing the properties
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    fileprivate func setupNavBarButtons() {
        // Adding search button to right nav bar item
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchForMovie))
        // Adding favourites button to left nav bar item
        let favouriteButton = UIButton()
        favouriteButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        favouriteButton.setImage(UIImage(named: "heart-on"), for: .normal)
        favouriteButton.addTarget(self, action: #selector(favouriteMovies), for: .touchUpInside)
        favouriteButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        favouriteButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        favouriteButton.translatesAutoresizingMaskIntoConstraints = false
        let barButtonLeft = UIBarButtonItem()
        barButtonLeft.customView = favouriteButton
        navigationItem.leftBarButtonItem = barButtonLeft
    }
    
    fileprivate func setupUI() {
        // URL to search for top movies
        guard let topMoviesSearchURL = URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=9e815367d4692859a98bd5557cb72440") else { return }
        // method to get the results and assign the results to the vairable
        FindMovie.fetchMovieData(topMoviesSearchURL, completionHandler: { movieSearch, Error in
            if let movieSearch = movieSearch {
                self.movieSearch = movieSearch
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    @objc func searchForMovie() {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "searchMoviesLayout") as? SearchMoviesTableViewController else {
            fatalError("Failed while casting the SearchMoviesTVC")
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func favouriteMovies() {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "FavouriteMoviesLayout") as? FavouriteMoviesTableViewController else {
            fatalError("Failed while casting the FavouriteMoviesTVC")
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movieSearch?.results.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "topRatedMoviescell", for: indexPath) as? TopMoviesTableViewCell else {
            fatalError("DequeueReusableCell failed while casting")
        }
        // string interpolation of imageURL
        guard let imageURL = URL(string: "https://image.tmdb.org/t/p/w185_and_h278_bestv2\(self.movieSearch?.results[indexPath.row].poster_path ?? "")") else { fatalError("Failed while casting image URL") }
        // using sdwebimage library to set the image to the imageView
        cell.moviePoster.sd_setImage(with: imageURL)
        // setting the movie title
        cell.movieName.text = self.movieSearch?.results[indexPath.row].original_title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "MovieDetailsLayout") as? MovieDetailsViewController else {
            fatalError("Failed while casting the MovieDetailsVC")
        }
        // passing the movieId from this ViewController to MovieDetailsViewController
        viewController.movieId = movieSearch?.results[indexPath.row].id ?? 0
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
