//
//  CoreDataManager.swift
//  Grindhouse Cinema
//
//  Created by Akarsh Seggemu on 20/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    /*
     The persistent container for the application. This implementation
     creates and returns a container, having loaded the store for the
     application to it. This property is optional since there are legitimate
     error conditions that could cause the creation of the store to fail.
     */
    let container: NSPersistentContainer
    
    override init() {
        // MovieModel is the name of the data model
        container = NSPersistentContainer(name: "MovieModel")
        container.loadPersistentStores { (NSPersistentStoreDescription, error) in
            guard error == nil else {
                print(error?.localizedDescription as Any)
                return
            }
        }
        super.init()
    }
    // saving the data using the save method
    private func save() {
        do {
            if container.viewContext.hasChanges {
                try container.viewContext.save()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    // Adding favourite movies to the database by storing
    // the movie id, movie name and movie poster url
    func addFavourite(by movieId: Int, name movieName: String, imageURL moviePoster: String) {
        let item = Favourite(context: container.viewContext)
        // movieId is converted into Int64 before setting it to item
        item.movieId = Int64(movieId)
        item.movieName = movieName
        item.moviePoster = moviePoster
        print(item)
        save()
    }
    // Checking if the movieId is present in the database or not
    func isFavourite(with identifier: Int) -> Bool {
        let managedContext = container.viewContext
        let request: NSFetchRequest<Favourite> = Favourite.fetchRequest()
        let predicate = NSPredicate(format: "movieId = %i", Int64(identifier))
        request.predicate = predicate
        // declaring the variable count and setting its default value to 0
        var count = 0
        do {
            count = try managedContext.count(for: request)
            // if the movieId is present it will return true
            // else false
            return count > 0
        } catch {
            fatalError("Failed to check whether the movied id is favourited: \(error)")
        }
    }
    // removing the movie from favourites by deleting the entire row
    // i.e. movieId, moveName and moviePoster
    func removeFavourite(by moviedId: Int) {
        let managedContext = container.viewContext
        let request: NSFetchRequest<Favourite> = Favourite.fetchRequest()
        let predicate = NSPredicate(format: "movieId = %i", Int64(moviedId))
        request.predicate = predicate
        do {
            let result = try managedContext.fetch(request)
            for object in result {
                managedContext.delete(object)
            }
            try managedContext.save()
        } catch {
            fatalError("Failed to remove favourite: \(error)")
        }
    }
    // gets the favourite movies and returns the favourite movies in an array
    func getFavourite(completionHandler: @escaping ([MovieFavourite]?, Error?) -> Void) {
        let managedContext = container.viewContext
        let request: NSFetchRequest<Favourite> = Favourite.fetchRequest()
        do {
            let results = try managedContext.fetch(request)
            var favourites = [MovieFavourite]()
            var favourite = MovieFavourite()
            for result in results {
                favourite.movieId = Int(result.movieId)
                favourite.movieName = result.movieName
                favourite.moviePoster = result.moviePoster
                favourites.append(favourite)
            }
            completionHandler(favourites, nil)
        } catch {
            completionHandler(nil, error)
            fatalError("Failed to get all favourites: \(error)")
        }
    }
}
