//
//  Grindhouse_CinemaUITests.swift
//  Grindhouse CinemaUITests
//
//  Created by Akarsh Seggemu on 17/04/2019.
//  Copyright © 2019-2021 Akarsh Seggemu. All rights reserved.
//

import XCTest

class Grindhouse_CinemaUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testForNavigationFromMovieToMovieDetails() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["君の名は。"]/*[[".cells.staticTexts[\"君の名は。\"]",".staticTexts[\"君の名は。\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["君の名は。"].buttons["Grindhouse Cinema"].tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["The Shawshank Redemption"]/*[[".cells.staticTexts[\"The Shawshank Redemption\"]",".staticTexts[\"The Shawshank Redemption\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["The Shawshank Redemption"].buttons["Grindhouse Cinema"].tap()
        
    }
    
    func testForNavigationToFavouriteMovies() {
        let app = XCUIApplication()
        app.navigationBars["Grindhouse Cinema"].buttons["heart on"].tap()
        app.navigationBars["Favourite Movies"].buttons["Grindhouse Cinema"].tap()
    }
    
    func testForNavigationToSearchMovies() {
        let app = XCUIApplication()
        let grindhouseCinemaNavigationBar = XCUIApplication().navigationBars["Grindhouse Cinema"]
        grindhouseCinemaNavigationBar.buttons["Search"].tap()
        grindhouseCinemaNavigationBar.buttons["Grindhouse Cinema"].tap()
    }
}
