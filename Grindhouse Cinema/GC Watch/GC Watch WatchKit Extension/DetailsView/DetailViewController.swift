//
//  DetailViewController.swift
//  GC Watch WatchKit Extension
//
//  Created by Akarsh Seggemu on 04.05.21.
//

import Foundation
import WatchKit

class DetailViewController: WKInterfaceController {
    @IBOutlet weak var movieImage: WKInterfaceImage!
    @IBOutlet weak var movieTitle: WKInterfaceLabel!
    @IBOutlet weak var movieOverview: WKInterfaceLabel!

    var name = String()

    private var movieDetails: [String: String] = ["Dilwale Dulhania Le Jayenge":"Raj is a rich, carefree, happy-go-lucky second generation NRI. Simran is the daughter of Chaudhary Baldev Singh, who in spite of being an NRI is very strict about adherence to Indian values. Simran has left for India to be married to her childhood fiancé. Raj leaves for India with a mission at his hands, to claim his lady love under the noses of her whole family. Thus begins a saga.",
        "Gabriel's Inferno Part II":"Professor Gabriel Emerson finally learns the truth about Julia Mitchell's identity, but his realization comes a moment too late. Julia is done waiting for the well-respected Dante specialist to remember her and wants nothing more to do with him. Can Gabriel win back her heart before she finds love in another's arms?",
        "The Shawshank Redemption":"Framed in the 1940s for the double murder of his wife and her lover, upstanding banker Andy Dufresne begins a new life at the Shawshank prison, where he puts his accounting skills to work for an amoral warden. During his long stretch in prison, Dufresne comes to be admired by the other inmates -- including an older prisoner named Red -- for his integrity and unquenchable sense of hope.",
        "The Godfather":"Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.",
        "Gabriel's Inferno Part III":"The final part of the film adaption of the erotic romance novel Gabriel's Inferno written by an anonymous Canadian author under the pen name Sylvain Reynard."]
    private let movieNames: [String: String] = ["Dilwale Dulhania Le Jayenge":"Dilwale Dulhania Le Jayenge", "Gabriel's Inferno Part II":"Gabriel's Inferno Part II", "The Shawshank Redemption":"The Shawshank Redemption", "The Godfather":"The Godfather", "Gabriel's Inferno Part III":"Gabriel's Inferno Part III"]
    private let movieImages: [String: String] = ["Dilwale Dulhania Le Jayenge":"a.jpg", "Gabriel's Inferno Part II":"b.jpg", "The Shawshank Redemption":"c.jpg", "The Godfather":"d.jpg", "Gabriel's Inferno Part III":"e.jpg"]


    override func awake(withContext context: Any?) {
        // Configure interface objects here.
        super.awake(withContext: context)
        name = context as! String
        movieTitle.setText(movieNames[name]!)
        movieImage.setImage(UIImage(named: movieImages[name]!))
        movieOverview.setText(movieDetails[name]!)

    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    @IBOutlet var goModalOutlet: WKInterfaceButton!

    @IBAction func goModalAction() {
        presentController(withName: "moreDetails", context: movieNames[name]!)
    }
}
