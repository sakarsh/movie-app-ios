//
//  InterfaceController.swift
//  GC Watch WatchKit Extension
//
//  Created by Akarsh Seggemu on 03.05.21.
//

import Foundation
import WatchKit

class InterfaceController: WKInterfaceController {
    @IBOutlet weak var movieTable: WKInterfaceTable!

    private let movieNames: [String] = ["Dilwale Dulhania Le Jayenge", "Gabriel's Inferno Part II", "The Shawshank Redemption", "The Godfather", "Gabriel's Inferno Part III"]
    private let movieImages: [String] = ["a.jpg", "b.jpg", "c.jpg", "d.jpg", "e.jpg"]


    override func awake(withContext context: Any?) {
        // Configure interface objects here.
        self.setupTableUI()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }

    func setupTableUI() {
        movieTable.setNumberOfRows(self.movieNames.count, withRowType: "MovieView")
        for (index, _) in self.movieNames.enumerated() {
            if let row = movieTable.rowController(at: index) as? MovieView {
                row.movieTitle.setText(self.movieNames[index])
                row.movieImage.setImage(UIImage(named: self.movieImages[index]))
            }
        }
    }

    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        self.pushController(withName: "showDetails", context: movieNames[rowIndex])
    }
}
