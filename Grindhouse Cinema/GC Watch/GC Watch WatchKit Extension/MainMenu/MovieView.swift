//
//  File.swift
//  GC Watch WatchKit Extension
//
//  Created by Akarsh Seggemu on 04.05.21.
//

import WatchKit

class MovieView: NSObject {
    @IBOutlet weak var movieImage: WKInterfaceImage!

    @IBOutlet weak var movieTitle: WKInterfaceLabel!
}
