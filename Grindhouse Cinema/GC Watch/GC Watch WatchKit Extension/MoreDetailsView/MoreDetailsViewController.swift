//
//  MoreDetailsViewController.swift
//  GC Watch WatchKit Extension
//
//  Created by Akarsh Seggemu on 04.05.21.
//

import Foundation
import WatchKit

class MoreDetailsViewController: WKInterfaceController {
    
    @IBOutlet weak var movieImage: WKInterfaceImage!
    @IBOutlet weak var movieTitle: WKInterfaceLabel!
    @IBOutlet weak var movieRating: WKInterfaceLabel!
    @IBOutlet weak var movieReleaseDate: WKInterfaceLabel!
    
    private let movieNames: [String: String] = ["Dilwale Dulhania Le Jayenge":"Dilwale Dulhania Le Jayenge", "Gabriel's Inferno Part II":"Gabriel's Inferno Part II", "The Shawshank Redemption":"The Shawshank Redemption", "The Godfather":"The Godfather", "Gabriel's Inferno Part III":"Gabriel's Inferno Part III"]
    private let movieImages: [String: String] = ["Dilwale Dulhania Le Jayenge":"a.jpg", "Gabriel's Inferno Part II":"b.jpg", "The Shawshank Redemption":"c.jpg", "The Godfather":"d.jpg", "Gabriel's Inferno Part III":"e.jpg"]
    private let movieRatings: [String: String] = ["Dilwale Dulhania Le Jayenge":"8.7", "Gabriel's Inferno Part II":"8.7", "The Shawshank Redemption":"8.7", "The Godfather":"8.7", "Gabriel's Inferno Part III":"8.7"]
    private let movieReleaseDates: [String: String] = ["Dilwale Dulhania Le Jayenge":"1995-10-20", "Gabriel's Inferno Part II":"2020-07-31", "The Shawshank Redemption":"1994-09-23", "The Godfather":"1972-03-14", "Gabriel's Inferno Part III":"2020-11-19"]

    override func awake(withContext context: Any?) {
        // Configure interface objects here.
        super.awake(withContext: context)
        let name = context as! String
        movieTitle.setText(movieNames[name]!)
        movieImage.setImage(UIImage(named: movieImages[name]!))
        movieRating.setText(movieRatings[name]!)
        movieReleaseDate.setText(movieReleaseDates[name]!)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    
}
