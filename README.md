# movie-app-ios

iOS client app called Grindhouse Cinema. It shows top rated movies in the first page. Movie details in the second page. The movies can be added to the favourite movies list by clickin on the "heart" icon in the movie details page. "Search" icon can be used to search for movies.

* API End point: `api.themoviedb.org`
* Architecture: MVP
* Programming language: Swift
* UI: Storyboard and UIKit
* Third party frameworks: Sourcery, SDWebImage

**Credits**

- Akarsh Seggemu
